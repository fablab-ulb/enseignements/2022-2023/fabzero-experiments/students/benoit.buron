# 2. Conception Assistée par Ordinateur (CAO)

## 1. Openscad

### C'est quoi ?
Openscad est un logiciel de création de modèle 3D se présantant comme ceci :  
![image de openscad](https://gitlab.com/fablab-ulb/enseignements/2022-2023/fabzero-experiments/students/benoit.buron/-/raw/main/docs/images/openscad.png)  
Il permet de créé des modèles 3D grâce à du code. Ce qui est très pratique car cela veut dire qu'on peut créé ce qu'on veut avec un simple éditeur de texte une fois le logiciel maitrisé.  
[Lien vers un tutoriel](https://openscad.org/documentation.html#tutorial)  
**Pense-bête**  
![image du pense-bête des commandes](https://gitlab.com/fablab-ulb/enseignements/2022-2023/fabzero-experiments/students/benoit.buron/-/raw/main/docs/images/openSCAD_cheat_sheet.png)

## 2. Modéles réalisés
Je me suis basé sur un fichier de base permettant de créé toutes les pièces LEGO voulues [LEGO.Scad](https://github.com/cfinke/LEGO.scad/blob/master/LEGO.scad)
### Modèle 1
![image du modèle 1](https://gitlab.com/fablab-ulb/enseignements/2022-2023/fabzero-experiments/students/benoit.buron/-/raw/main/docs/images/mod%C3%A8le%201.png)
### Modèle 2
![image du modèle 2](https://fablab-ulb.gitlab.io/enseignements/2022-2023/fabzero-experiments/students/donovan.crousse/images/curve-original.jpg)

## 3. Creative Commons License
Quand on créé quelque choses sur internet, il faut y associer une licence Creative Commons car cela permet de protéger ses droits d'auteurs tout en permettant sa diffusion, [plus d'info ici](https://fr.wikipedia.org/wiki/Licence_Creative_Commons).  
Le tableau ci dessous reprend les différentes catégories.  
![tableau](https://gitlab.com/fablab-ulb/enseignements/2022-2023/fabzero-experiments/students/benoit.buron/-/raw/main/docs/images/tableau%20license.png)  
*Légende :*  
Attribution [BY] (Attribution) : l'œuvre peut être librement utilisée, à la condition de l'attribuer à l'auteur en citant son nom. Cela ne signifie pas que l'auteur est en accord avec l'utilisation qui est faite de ses œuvres.

Pas d'utilisation commerciale [NC] (Noncommercial) : le titulaire de droits peut autoriser tous les types d’utilisation ou au contraire restreindre aux utilisations non commerciales. Elle autorise à reproduire, diffuser, et à modifier une œuvre, tant que l'utilisation n'est pas commerciale.

Pas de modification [ND] (NoDerivs) : le titulaire de droits peut continuer à réserver la faculté de réaliser des œuvres de type dérivées ou au contraire autoriser à l'avance les modifications, traductions.

Partage dans les mêmes conditions [SA] (ShareAlike) : le titulaire des droits peut autoriser à l'avance les modifications ; peut se superposer l'obligation (SA) pour les œuvres dites dérivées d'être proposées au public avec les mêmes libertés que l'œuvre originale.

Zéro : le créateur renonce à ses droits patrimoniaux. Aucune limite à la diffusion de l'œuvre n'existe, sous réserve des législations locales. Dans un certain nombre d’États, la licence CC0 équivaut à la licence CC-BY. 