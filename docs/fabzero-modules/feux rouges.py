import machine, neopixel
np = neopixel.NeoPixel(machine.Pin(23), 8)
import time
while True: # boucle se répétant à l'infini
    np[0] = (0, 255, 0) # passe au vert
    np.write()
    time.sleep(1) # attend 1 seconde avant de continuer à lire le programme
    np[0] = (255, 100, 0) # passe à l'orange
    np.write()
    time.sleep(1)
    np[0] = (255, 0, 0) # passe au rouge
    np.write()
    time.sleep(1)