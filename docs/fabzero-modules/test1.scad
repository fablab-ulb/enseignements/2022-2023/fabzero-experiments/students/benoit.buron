use <LEGO.scad>;

rotate([0, 0, 180]) union() {
    color("gray") place(0, -14) uncenter(-4, 0) rotate([0, 0, 90]) block(
        type="brick",
        roadway_invert=true,
        width=4,
        length=3,
        height=1/3,
        roadway_width=1,
        roadway_length=2,
        roadway_y=1,
        roadway_x=1
    );
}