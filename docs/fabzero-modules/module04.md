# 4. Outil Fab Lab sélectionné

## Les microcontroleurs

Ce sont des ordinateurs miniatures ne contenant que l'essentiel, sur lesquel on peut exécuter du code et qui grâce à une association à des modules permet beaucoup de choses.  
Ils sont très utile et utilisé dans l'industrie.  
Ils sont très pratique car ils coutent peu cher, ne prenne pas bcp de place et permettent beaucoup grâce à la variété de modules associables (mesures en tout genre, allumage de panneaux LED, moteur, etc).  
Le microcontroleur utilisé est un Raspberry Pi Pico (YD-RP2040), [**fiche technique**](https://fablab-ulb.gitlab.io/enseignements/fabzero/electronics/).

## Choix du language de programation
Le microcontrolleur est capable de comprendre plusieurs language de programation
l'Arduino, le Phyton et le C.  
Il convient au programateur de choisir le language qu'il veut en fonction de ses compétences et de la tache à réaliser.
[Librairie phyton](https://docs.micropython.org/en/latest/index.html)

### Exemples de contrôle de la LED
La LED étant une RGB, elle utilise les 3 couleurs primaires selon une intesité allant de 0 à 255. C'est en combinant celles-ci qu'on crée toutes les autres couleurs. [Code RGB](https://www.rapidtables.com/web/color/RGB_Color.html)

[**Lien vers le code pour le Contrôle de la LED principale**](https://gitlab.com/fablab-ulb/enseignements/2022-2023/fabzero-experiments/students/benoit.buron/-/blob/main/docs/fabzero-modules/no%20light.py)  
![no light](https://gitlab.com/fablab-ulb/enseignements/2022-2023/fabzero-experiments/students/benoit.buron/-/raw/main/docs/images/no%20light.png)
[**Lien vers le code pour le Feux rouges**](https://gitlab.com/fablab-ulb/enseignements/2022-2023/fabzero-experiments/students/benoit.buron/-/blob/main/docs/fabzero-modules/feux%20rouges.py)  
![feux rouges](https://gitlab.com/fablab-ulb/enseignements/2022-2023/fabzero-experiments/students/benoit.buron/-/raw/main/docs/images/feux%20rouges.png)  

[**vidéo du feux rouges**](https://gitlab.com/fablab-ulb/enseignements/2022-2023/fabzero-experiments/students/benoit.buron/-/raw/main/docs/images/20230314_140847.mp4)  

[**Lien vers le code pour le Disco**](https://gitlab.com/fablab-ulb/enseignements/2022-2023/fabzero-experiments/students/benoit.buron/-/blob/main/docs/fabzero-modules/couleur%20from%20futur.py)  
![disco](https://gitlab.com/fablab-ulb/enseignements/2022-2023/fabzero-experiments/students/benoit.buron/-/raw/main/docs/images/disco.png)  
[**vidéo du disco**](https://gitlab.com/fablab-ulb/enseignements/2022-2023/fabzero-experiments/students/benoit.buron/-/raw/main/docs/fabzero-modules/images/20230417_011246.mp4)
### DHT20 
Pour ce module il faut au préalable téléchargé le fichier [dht20.py](https://gitlab.com/fablab-ulb/enseignements/2022-2023/fabzero-experiments/students/benoit.buron/-/blob/main/docs/fabzero-modules/dht20.py)  
Ce module permet la mesure de la température en °C et de l'humidité.
Le programme que j'ai codé fait que la Led change de couleur en fonction des mesures :  
* rouge = chaud et sec  
* vert = chaud et humide  
* bleu = froid et sec  
* rose = froid et humide

[*lien vers le code*](https://gitlab.com/fablab-ulb/enseignements/2022-2023/fabzero-experiments/students/benoit.buron/-/blob/main/docs/fabzero-modules/capteur%20de%20temperature%20%2B%20allumage%20lumi%C3%A8re.py)  
![image code dht20](https://gitlab.com/fablab-ulb/enseignements/2022-2023/fabzero-experiments/students/benoit.buron/-/raw/main/docs/images/dht20.png)  

Pour pouvoir utiliser ce module il faut le brancher correctement pour cela les informations sur quel branche correspond à quoi se trouvent dans la [fiche technique de dht20](https://cdn.sparkfun.com/assets/8/a/1/5/0/DHT20.pdf)  
![branchement](https://gitlab.com/fablab-ulb/enseignements/2022-2023/fabzero-experiments/students/benoit.buron/-/raw/main/docs/images/branchement.png).  
![schéma du branchement](https://gitlab.com/fablab-ulb/enseignements/2022-2023/fabzero-experiments/students/benoit.buron/-/raw/main/docs/images/Sans%20titre.jpg)  

![branchement du dht20](https://gitlab.com/fablab-ulb/enseignements/2022-2023/fabzero-experiments/students/benoit.buron/-/raw/main/docs/fabzero-modules/images/20230417_011331.jpg)