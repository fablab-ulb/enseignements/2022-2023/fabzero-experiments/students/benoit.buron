import machine, neopixel # permet d'utiliser la LED
np = neopixel.NeoPixel(machine.Pin(23), 8)  # dit au microcontrolleur où se trouve le LED sur celui-ci
import time  # permet d'incorporer une notion de temps dans le programme
np[0]=(0, 0, 0) # change la couleur de le LED, ici ça l'éteint 
np.write() # exécute le changement