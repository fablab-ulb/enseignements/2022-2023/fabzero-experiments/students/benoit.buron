import machine, neopixel
from machine import Pin
np = neopixel.NeoPixel(machine.Pin(23), 8)
import time

while True :
    np[0]=(100, 20, 20)
    np.write()
    time.sleep_ms(60)
    np[0]=(0, 0, 0)
    np.write()
    time.sleep_ms(60)
    np[0]=(0, 255, 0)
    np.write()
    time.sleep_ms(60)
