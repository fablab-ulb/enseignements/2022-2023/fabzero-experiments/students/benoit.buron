## Gestion de projet et documentation

## 1. But
 
 Le but de la documentation du projet est de permettre a n' importe qui, qui tombe sur le sur le site de comprendre ce qu'on a fait, comment on l'a fait et d'être capable d'utiliser celà dans un nouveau projet.
 
 Dans ce but on utilisera plusieurs outils différents qui seront expliqué au cours de cette documentation 
## 2. Tuto bash
**Premièrement pourquoi Bash?** 

Bash est un loigiciel d'exploration de fichier, il permet d'accéder rapidement et efficacement à ses fichiers et d'en faire ce qu'on veut. 

Il permet aussi de pouvoir travailler sans besoin d'internet et permet de mettre à jour nos avancements pour qu'ils soient disponible à tous les collaborateurs, dès le retour d'une connexion Internet.

**Comment l'utiliser ?** 

Bash ce présent sous la forme d'un terminal (n'ayez pas peur).

![image console](https://gitlab.com/fablab-ulb/enseignements/2022-2023/fabzero-experiments/students/benoit.buron/-/raw/main/docs/images/Capture%20d%E2%80%99%C3%A9cran%202023-03-15%20143906.jpg)

Ce terminal s'utilise en tapant des commandes, La plus importante étant help (ou --help après une autre commande) qui permet d'afficher toutes les options/ commandes disponibles et comment les utilisées. 

![image de help](https://gitlab.com/fablab-ulb/enseignements/2022-2023/fabzero-experiments/students/benoit.buron/-/raw/main/docs/images/Capture%20d%E2%80%99%C3%A9cran%202023-03-15%20144237.jpg)

Liste des commandes les plus utiles :

* ' cd '   permet d'acceder a un dossier en écrivant son nom après cd. 
 
  * ' cd - '   permet de revenir au dossier précédant

* ' ls -lh '   permet de lister les fichier / dossier présent dans le dossier

* ' mkdir '   permet de crée un dossier où on se trouve (pas oublier de le nommer) 

* ' touch '   crée un fichier texte vide (pas oublier de le nommer)

* ' rm '   permet de supprimer un fichier / dossier

* ' mv '   permet de déplacer un fichier (source --> destination (permet de renommer un fichier si il est envoyé dans le même dossier))

* ' cat '   permet d'afficher le contenu d'un fichier dans le terminal

* ' echo '   permet d'écrire dans un fichier ( avec ' > ' supprime le contenu du fichier puis écrit, avec ' >> ' écrit dans le fichier sans effacer le contenu)  
![image de chaque commande](https://gitlab.com/fablab-ulb/enseignements/2022-2023/fabzero-experiments/students/benoit.buron/-/raw/main/docs/images/Capture%20d%E2%80%99%C3%A9cran%202023-03-15%20145504.jpg) \

(*astuce*: appuyer sur tab rempli directement a notre place quand on tape le nom des fichiers)

Liste des commandes pour git : 

* ' git status ' permet de savoir quel est le status des fichiers sur le pc comparé sur le site de gitlab 
 (*astuce*: quand on utilise les commandes git, le terminal nous propose directement les commandes possible)

* ' git add ' met à jour le fichier

* ' git restore ' restaure l'état antérieur du fichier

* ' git push ' envois les fichiers sur git lab

* ' git commit ' met à jour tous les fichiers du dossier sur git 

![image des commandes git](https://gitlab.com/fablab-ulb/enseignements/2022-2023/fabzero-experiments/students/benoit.buron/-/raw/main/docs/images/Capture%20d%E2%80%99%C3%A9cran%202023-03-15%20162846.jpg)

## 3. Tuto page web

Pour la création d'une page internet on utilise le markdown car c'est un language simple et très proche de l'écrit, le language sera par la suite traduit en html par un convertisseur.

**Apprendre le markdown simplement.** 

Pour écrire des lignes de texte, écrivez simplement normalement comme dans un fichier texte

Pour mettre en gras, entourrez le texte à mettre en gras de \*\* ** ou \_\_ \_\_ .  
Pour de l'italique, entourrez le texte à mettre en gras de \*\* ou \_ \_  (vous pouvez mixez l'italique et le gras avec \*\_ \_\* ou \_\* \*\_).

Pour les titres, mettez des ' # '  devant plus il y a de # plus le titre sera petit (jusqu'a 6 # max).

Pour faire des paragraphes, laissez un ligne d'écart entre les lignes de texte.  
Pour aller à la ligne, mettez laissez 2 espaces puis appuyer sur **enter** puis allez à la ligne.

Pour des citations mettez des ' > ' devant chaque ligne de la citation.

Pour des listes ordonné, utilisez 1. 2. , ...  
Pour des listes pas ordonné utilisez des tirets.

Pour des liens vers des pages internet, utilisez ' [textes qui sera le lien sur lequel clicker]\(lien de la page internet) '.

Pour des images, même chose mais avec un ' ! ' devant les crochets (utilisez des .png).  
Pareil avec des vidéos mais utilisez des .gif ou .mp4, pour des musique du .mp3 (en bref utilisez pas des trucs chelous, et des images/video pas trop lourds).

**Modifications des images**

Si vous images sont trop grandes utilisez paint pour les redimensioner.  
Vous pouvez faire des collages, etc...

**Comment utiliser ces images modifié**

Enregistrez les images modifié dans votre dossier git, faites un commit.  
Allez sur votre page git, copiez le lien de l'image et insérer le à la place du lien initial. 

**Pour éditer le site**

Il suffit de mettre en pratique ce qui a été vu plus haut.  
En écrivant dans le fichier index.md pour la premiére page et dans les fichiers modules.md pour les modules ou plus généralement dans un fichier en .md .

Vous pouvez aussi écire dans ces fichiers grâce a Bash ou en utilisant un logiciel, puis il ne vous reste plus qu'à faire un commit.
