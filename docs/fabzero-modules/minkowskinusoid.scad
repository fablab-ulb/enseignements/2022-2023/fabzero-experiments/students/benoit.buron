// Define the dimensions of the objects


module minkowskinusoid(width
,height
,blade_thickness
,blade_length){
    
    cube_width = width;
cube_depth = width;
cube_height = height;

//cylinder_radius = width/sqrt(2);
cylinder_radius = width/2;
cylinder_height = height;
resolution = width*200;
    
    // Subtract the cylinder from the cube
    difference() {
        cube([cube_width
        , cube_depth
        , cube_height]);
        
        translate([width
        ,0-blade_thickness
        ,-height/10])
        
        cylinder(r=cylinder_radius+blade_thickness/2
        , h=cylinder_height+height/5
        , $fn = resolution);
        
//        cube([cube_width
//        , cube_depth
//        , cube_height]);
//        
        translate([width
        ,width+blade_thickness
        ,-height/10])
        
        cylinder(r=cylinder_radius+blade_thickness/2
        , h=cylinder_height+height/5
        , $fn = resolution);
    }

    translate([-width
    ,0
    ,0])

    minkowski(){
        cube([cube_width
        , cube_depth
        , cube_height-height/2]);
        
        cylinder(r=cylinder_radius
        , h=height/2
        , $fn = resolution);
    }
}

minkowskinusoid(width=10
,height=20
,blade_thickness=2);