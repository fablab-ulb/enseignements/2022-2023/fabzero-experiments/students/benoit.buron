from machine import Pin, I2C
from utime import sleep
import time
from dht20 import DHT20

import machine, neopixel

np = neopixel.NeoPixel(machine.Pin(23), 8)

i2c0_sda = Pin(0)
i2c0_scl = Pin(1)
i2c0 = I2C(0, sda=i2c0_sda, scl=i2c0_scl)
dht20= DHT20(0x38, i2c0)
measurements=dht20.measurements
a=int(measurements['t'])
b=int(measurements['rh'])
while True:
    if a > 20:
        np[0]=(a, 0, 0)
        np.write()
        if b > 37:
            np[0]=(a, b, 0)
            np.write()
            print("La température est",a,"°C", ","" L'humidité est",b,"%RH")
        else:
            np[0]=(a, 0, 0)
            np.write()
            print("La température est",a,"°C", "," "L'humidité est",b,"%RH")
    else:
        np[0]=(0, 0, a)
        np.write()
        print("La température est",a,"°C","," "L'humidité est",b,"%RH")
        if b > 37:
            np[0]=(b, 0, a)
            np.write()
            print("La température est",a,"°C", ",""L'humidité est",b,"%RH")
        else:
            np[0]=(0, 0, a)
            np.write()
            print("La température est",a,"°C","," "L'humidité est",b,"%RH")
    time.sleep(5)