# 3. Impression 3D

L'impression 3D est un outil formidable mais il n'est pas parfait et comporte plusieurs limitations :  
* La maîtrise des logiciels  
* Matériaux, tailles et forme limités  
* Impacte sur l'environnement  
* Limites physiques  
* cout élévés pour des pièces grandes ou massives  

Il est possible de tester les limites de son imprimante 3D grâce à des torture test: ![exemple de torture test](https://cdn.thingiverse.com/renders/af/f2/3e/30/68/2caec0c3a6f8bbe39f870c0eb585298b_preview_featured.JPG)
![exemple de torture test](https://cdn.thingiverse.com/renders/62/ab/d7/e3/ea/1_3D-printed_3DBenchy_by_Creative-Tools.com_preview_featured.JPG)
## PrusaSlicer
Ce logiciel permet l'impression 3D car il convertit notre modèle 3D en un fichier lisible par l'imprimente lui permettant de faire son boulot. 
![image du prusa](https://gitlab.com/fablab-ulb/enseignements/2022-2023/fabzero-experiments/students/benoit.buron/-/raw/main/docs/images/Prusa%201.png)
Il permet aussi de visualiser la pièce couche par couche, de l'agrandir, etc ; ainsi que d'apporter des modifications à celle-ci. 
Il permet de régler les détails de l'impression : épaisseur de l'impression, sélectionner le bon modèle d'imprimante, etc.
Voici une pièce réalisé par nos soins.
![image d'une pièce 3d](https://fablab-ulb.gitlab.io/enseignements/2022-2023/fabzero-experiments/students/noe.bourgeois/fabzero-modules/module03/THCFER/image/PXL_20230315_102342198.jpg)