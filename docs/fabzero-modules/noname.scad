$fn=150;
side=10;
difference(){
    cube([side*10,side,side],center=true);
    cube([(side*10)-1,side-1,side],center=true);}
translate([0,side/4,0])cylinder(side-4,side/10);    
translate([0,-(side/4),0])cylinder(side-4,side/10);