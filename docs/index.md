### Moi

Salut, je m'appelle Benoît, j'ai 20 ans, je suis en Ba3 biologie.

### Moi encore plus

Je suis Carolo ( Mes couilles ti !).  
J'adore les mangas et la bande-dessinée en générale, mon manga préférer c'est JoJo Bizar Aventure et Berzerk.

![image de jojo](https://upload.wikimedia.org/wikipedia/fr/f/fe/Jojo%27s_Bizarre_Adventure_%28jeu_vid%C3%A9o%29_Logo.jpg?20101030194143)
![image de berzerk](https://gitlab.com/fablab-ulb/enseignements/2022-2023/fabzero-experiments/students/benoit.buron/-/raw/main/docs/images/0778b5ad3fd1678123ba77f0384adb11.jpg)

Je suis un grand fan de jeux-video, mon petit top 5 c'est : 

* Skyrim (évidement),
* Borderlands 2 (j'y suis accro, j'ai fini le jeu 5 fois), 
* Pokemon blanc 2 (et jaune 2(mdr)), 
* Mario Galaxy 2 (toute mon enfance), 
* Monster Hunter World (500 heures de chasse de Wyvern).

![image de skyrim](https://gitlab.com/fablab-ulb/enseignements/2022-2023/fabzero-experiments/students/benoit.buron/-/raw/main/docs/images/725px-SR-cover-Skyrim_Box_Art.jpg)
![image de bordelands](https://png.pngitem.com/pimgs/s/181-1815199_borderlands-2-logo-png-borderlands-2-logo-transparent.png)
![image d'oeuf pour faire comprendre la blague](https://gitlab.com/fablab-ulb/enseignements/2022-2023/fabzero-experiments/students/benoit.buron/-/raw/main/docs/images/eR0nDTWJIpsF.jpg)
![image de mario galaxy 2](https://gitlab.com/fablab-ulb/enseignements/2022-2023/fabzero-experiments/students/benoit.buron/-/raw/main/docs/images/Super_Mario_Galaxy_2_Logo.jpg)
![image de MHW](https://upload.wikimedia.org/wikipedia/en/1/1b/Monster_Hunter_World_cover_art.jpg)

Sinon la bouffe c'est la vie et je cuisine bien en plus (époussez-moi !)
Et pour finir les plantes c'est trop cool, les requins aussi.

